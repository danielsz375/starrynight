﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarryNight.Model
{
    class BeeMovedEventArgs : EventArgs 
    {
        public Bee BeeThatMoved { get; private set; }

        public double X { get; set; }
        public double Y { get; set; }

        public BeeMovedEventArgs(Bee beeThatMoved, double x, double y)
        {
            BeeThatMoved = beeThatMoved;
            X = x;
            Y = y;
        }
    }
}
