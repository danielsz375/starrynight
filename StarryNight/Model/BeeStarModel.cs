﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace StarryNight.Model
{
    class BeeStarModel
    {
        public static readonly Size StarSize = new Size(150, 100);

        private readonly Dictionary<Bee, Point> _bees = new Dictionary<Bee, Point>();
        private readonly Dictionary<Star, Point> _stars = new Dictionary<Star, Point>();
        private Random _random = new Random();

        public event EventHandler<StarChangedEventArgs> StarChanged;

        private void OnStarChanged(Star starThatChanged, bool removed)
        {
            EventHandler<StarChangedEventArgs> starChangedEvent = StarChanged;
            if (starChangedEvent != null)
            {
                starChangedEvent(this, new StarChangedEventArgs(starThatChanged, removed));
            }
        }

        public event EventHandler<BeeMovedEventArgs> BeeMoved;

        private void OnBeeMoved(Bee beeThatMoved, double x, double y)
        {
            EventHandler<BeeMovedEventArgs> beeMovedEvent = BeeMoved;
            if (beeMovedEvent != null)
            {
                beeMovedEvent(this, new BeeMovedEventArgs(beeThatMoved, x, y));
            }
        }


        public BeeStarModel()
        {
            PlayAreaSize = Size.Empty;
        }

        public void Update()
        {
            MoveOneBee();
            AddOrRemoveAStar();
        }

        private static bool RectsOverlap(Rect r1, Rect r2)
        {
            r1.Intersect(r2);
            if (r1.Width > 0 || r1.Height > 0)
                return true;
            return false;
        }

        private Size _playAreaSize;
        public Size PlayAreaSize 
        {
            get
            {
                return _playAreaSize;
            }

            set 
            {
                _playAreaSize = value;
                CreateBees();
                CreateStars();
            } 
        }

        private void CreateBees()
        {
            if (PlayAreaSize == Size.Empty)
                return;

            if (_bees.Count > 0)
            {
                foreach (Bee bee in _bees.Keys.ToList())
                    MoveOneBee(bee);
            }
            else
            {
                int numberOfBeesToCreate = _random.Next(5, 15);
                for (int i = 0; i < numberOfBeesToCreate; i++)
                {
                    int width = _random.Next(40, 150);
                    int height = width;
                    Size size = new Size(width, height);

                    Point location = FindNonOverlappingPoint(size);

                    Bee newBee = new Bee(location, size);
                    _bees.Add(newBee, location);

                    OnBeeMoved(newBee, location.X, location.Y);
                }
                
            }

        }

        private void CreateStars()
        {
            if (PlayAreaSize == Size.Empty)
                return;

            if (_stars.Count > 0)
            {
                foreach (Star star in _stars.Keys)
                {
                    Point point = FindNonOverlappingPoint(StarSize);
                    star.Location = point;

                    OnStarChanged(star, false);
                }
            }
            else
            {
                int numberOfStarsToCreate = _random.Next(5, 10);
                for (int i = 0; i < numberOfStarsToCreate; i++)
                {
                    CreateAStar();
                }
            }

        }

        private void CreateAStar()
        {
            Point point = FindNonOverlappingPoint(StarSize);
            Star star = new Star(point);
            _stars.Add(star, point);

            OnStarChanged(star, false);
        }

        private Point FindNonOverlappingPoint(Size size)
        {
            Rect newRect;
            Point location;
            int numberOfCollidingBees, numberOfCollidingStars;
            int count = 0;
            bool noOverlap = false;
            do
            {
                //int x = _random.Next(0, (int)(PlayAreaSize.Width - 150));
                int x = _random.Next(0, (int)(PlayAreaSize.Width - size.Width));
                //int y = _random.Next(0, (int)(PlayAreaSize.Height - 150));
                int y = _random.Next(0, (int)(PlayAreaSize.Height - size.Height));
                location = new Point(x, y);
                newRect = new Rect(location, size);

                var collidingBees =
                    from bee in _bees.Keys
                    where RectsOverlap(bee.Position, newRect)
                    select bee;
                numberOfCollidingBees = collidingBees.ToList().Count;

                var collidingStars =
                    from star in _stars.Keys
                    where RectsOverlap(
                        new Rect(star.Location.X, star.Location.Y, StarSize.Width, StarSize.Height), 
                        newRect)
                    select star;
                numberOfCollidingStars = collidingStars.ToList().Count;

                if ((collidingBees.Count() + collidingStars.Count() == 0) || (count++ > 1000))
                    noOverlap = true;
            } while (!noOverlap);

            return new Point(newRect.X, newRect.Y);
        }

        private void MoveOneBee(Bee bee = null)
        {
            if (_bees.Keys.Count == 0) return;

            if (bee == null)
            {
                bee = _bees.Keys.ToList()[_random.Next(_bees.Count)];
            } 
            else
            {
                //Nie jest jasno określone jak 'zaaktualizować' słownik pszczół
                //Zatem usuwamy starą pszczołę i wstawiamy nową, z nową lokalizacją
                _bees.Remove(bee);
            }

            bee.Location = FindNonOverlappingPoint(bee.Size);
            _bees[bee] = bee.Location;

            OnBeeMoved(bee, bee.Location.X, bee.Location.Y);
        }

        private void AddOrRemoveAStar()
        {
            if (((_random.Next(2) == 0) || (_stars.Count <= 5)) && (_stars.Count < 20))
                CreateAStar();
            else
            {
                Star starToRemove = _stars.Keys.ToList()[_random.Next(_stars.Count)];
                _stars.Remove(starToRemove);
                OnStarChanged(starToRemove, true);
            }
        }
    }
}
